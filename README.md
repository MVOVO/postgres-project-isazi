# Postgres Project Isazi

To run the project in Linux, change the "install.sh" script to be an executable as follows:

`sudo chmod +x install.sh`

Then run the script to install both databases:

`./install.sh`

Run the following command to check if the dbs are running:
`docker ps`

Expected Outcome:

`CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS                PORTS                     NAMES`

`fd6cf9f11af2        postgres:latest      "docker-entrypoint.s…"   29 minutes ago      Up 29 minutes         0.0.0.0:15433->5432/tcp   replica-db`
`2ca74fcbebf5        postgres:latest      "docker-entrypoint.s…"   31 minutes ago      Up 29 minutes         0.0.0.0:15432->5432/tcp   master-db`
