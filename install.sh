#!/bin/bash -x

# create network called "test"
docker network create test -d overlay --attachable

# Master-db
# create master-db and replica-db, and then create a database called "test" on both dbs
docker run -d --name master-db -p 15432:5432 --network test -v $PWD:/var/lib/postgresql/data -e POSTGRES_HOST_AUTH_METHOD=trust postgres:latest

docker exec -it master-db sed -i s#peer#trust#g /var/lib/postgresql/data/.docker/Volumes/gitlab-data-gitlab-0/pvc-f977fbe8-d3c5-11e9-a6f5-025000000001/postgresql/data/pg_hba.conf
docker exec -it master-db pg_createcluster --start 13 main
docker exec -it -u postgres master-db createdb test

# Replica-db
docker run -d --name replica-db -p 15433:5432 --network test -v $PWD:/var/lib/postgresql/data -e POSTGRES_HOST_AUTH_METHOD=trust postgres:latest
docker exec -it replica-db sed -i s#peer#trust#g /var/lib/postgresql/data/.docker/Volumes/gitlab-data-gitlab-0/pvc-f977fbe8-d3c5-11e9-a6f5-025000000001/postgresql/data/pg_hba.conf
docker exec -it replica-db pg_createcluster --start 13 main
docker exec -it -u postgres replica-db createdb test
